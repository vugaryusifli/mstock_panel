$(function () {

    $('#toggle').on('click', function (event) {
        event.stopPropagation();
        $('#sideBar').toggleClass('active');
        $('#mainArea').toggleClass('add');
        $('.sideBarItem').toggleClass('move');
        $('#logo').toggleClass('size');
        $('#body').toggleClass('scroll');
        $('.overlayMainContent').toggleClass('overlay');
        $('.sideBarItemRight').toggleClass('show');
        $('.sideBarItemLeft img').toggleClass('spin');
    });

    $('.overlayMainContent').on('click',function () {
        $('#sideBar').removeClass('active');
        $('#mainArea').removeClass('add');
        $('.sideBarItem').removeClass('move');
        $('#logo').removeClass('size');
        $('#body').removeClass('scroll');
        $('.overlayMainContent').removeClass('overlay');
        $('.sideBarItemRight').removeClass('show');
        $('.sideBarItemLeft img').removeClass('spin');
    });

    let modalBtn = document.getElementById("modal-btn");
    let modalBtn2 = document.getElementById("modal-btn2");
    let modal = document.querySelector(".modal");
    let closeBtn = document.querySelector(".close");

    modalBtn.onclick = function(){
        modal.style.display = "flex";
        document.body.classList.add("open-modal");
    };
    modalBtn2.onclick = function(){
        modal.style.display = "flex";
        document.body.classList.add("open-modal");
    };
    closeBtn.onclick = function(){
        modal.style.display = "none";
        document.body.classList.remove("open-modal");
    };
    window.onclick = function(e){
        if(e.target === modal){
            modal.style.display = "none";
            document.body.classList.remove("open-modal");
        }
    }

});



